class Foo
{
public:

    int a;
    float b;

    Foo(int a1, float b1);

    void Add( Foo* foo2);

    virtual void print();
    virtual void AbstractPrint2() = 0;
};
