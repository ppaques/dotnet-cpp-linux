﻿using System;
using System.Runtime.InteropServices;
using Sample;

namespace csharp
{
    class Program
    {


        // [DllImport(@"../cpp_proj/libhello-cpp.so")]
        // public static extern void PrintHelloWorld();

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //PrintHelloWorld();

            // Now lets try to usa Sample!
            Foo sample1 = new FooCsharp(1, 0.4f);
            Foo sample2 = new FooCsharp(2, 1.6f);

            sample1.Print();
            sample2.Print();

            sample1.Add(sample2);
            sample1.Print();
            sample2.AbstractPrint2();

        }
    }
}
