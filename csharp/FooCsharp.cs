using System;

namespace Sample
{

    public class FooCsharp : Foo
    {
        public FooCsharp(int a, float b) : base(a, b)
        {

        }

        public override void AbstractPrint2()
        {
            Console.WriteLine(string.Format("FooPrincC:{0},{1}", A, B));
        }
    }

}