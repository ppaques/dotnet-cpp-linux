using CppSharp;
using CppSharp.AST;
using CppSharp.Generators;

class DllDemoGenerator : ILibrary
{
    public void Setup(Driver driver)
    {
        var options = driver.Options;
        options.GeneratorKind = GeneratorKind.CSharp;
        options.GenerateObjectOverrides = true;
        options.CompileCode = true;
        options.OutputDir = "bindings";

        var module = options.AddModule("Sample");
        module.IncludeDirs.Add("../cpp_proj");
        module.Headers.Add("Sample.h");
        module.LibraryDirs.Add("../cpp_proj");
        module.Libraries.Add("libsample-cpp.so");
    }

    public void SetupPasses(Driver driver){}

    public void Preprocess(Driver driver, ASTContext lib){}

    public void Postprocess(Driver driver, ASTContext lib){}
}
